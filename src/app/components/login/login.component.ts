import {Component} from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  private username: string = '';
  private MIN_USERNAME_LENGTH: number = 3;

  constructor(private userService: UserService,
              private router: Router) {
  }

  send(e) {
    e.preventDefault();
    this.userService.setCurrentUser(this.username);
    this.router.navigate(['/chat']);
  }
}
