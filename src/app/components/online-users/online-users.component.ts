import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChatService} from "../../services/chat.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'online-users',
  templateUrl: './online-users.component.html',
  styleUrls: ['./online-users.component.css']
})
export class OnlineUsersComponent implements OnInit, OnDestroy {

  private users = [];
  private receiver;
  private connection;

  constructor(private chatService: ChatService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.receiver = this.chatService.receiver;
    this.connection = this.chatService.getUsers()
      .subscribe((users: Array<any>) => {
        this.users = users.filter(user => this.userService.getCurrentUser().name !== user.name);
      });
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
    this.chatService.receiver = {};
  }

  changeReceiver(name) {
    this.receiver['name'] = name;
    this.chatService.loadMessageHistory();
  }
}
