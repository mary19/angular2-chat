import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChatService} from "../../services/chat.service";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit, OnDestroy {

  private message;
  private messages = [];
  private connection;
  private receiver;

  constructor(private chatService: ChatService) {
  }


  ngOnInit(): void {
    this.receiver = this.chatService.receiver;
    this.connection = this.chatService.getAllMessages()
      .subscribe(messages => this.messages = messages);
  }

  ngOnDestroy(): void {
    this.connection.unsubscribe();
  }

  sendMessage(): void {
    this.chatService.sendMessage(this.message);
    this.message = '';
  }

}
