import {Component, OnInit} from '@angular/core';
import {ChatService} from "../../services/chat.service";

@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  private areUsers: boolean;

  constructor(private chatService: ChatService) {
  }

  ngOnInit() {
    this.chatService.getUsers()
      .subscribe((users: Array<any>) => this.areUsers = users.length > 1);
  }

}
