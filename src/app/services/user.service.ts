import {Injectable} from '@angular/core';
import {CanActivate} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {environment} from '../../environments/environment';
import * as io from 'socket.io-client';

@Injectable()
export class UserService implements CanActivate {

  private currentUser = null;
  private socket;

  constructor() {
    this.socket = io(environment.baseUrl);
  }

  canActivate(): Observable<boolean> | boolean {
    return !!this.currentUser;
  }

  setCurrentUser(name) {
    this.socket.emit('user-enter', {name});
    this.currentUser = {name};
  }

  removeCurrentUser(): void {
    this.currentUser = null;
  }

  getCurrentUser() {
    return this.currentUser;
  }

}
