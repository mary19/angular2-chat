import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Observable} from "rxjs/Observable";
import * as io from 'socket.io-client';
import {UserService} from "./user.service";

@Injectable()
export class ChatService {

  private socket;
  public receiver = {};

  constructor(private userService: UserService) {
    this.socket = io(environment.baseUrl);
  }

  sendMessage(message): void {
    this.socket.emit('add-message', {
      from: this.userService.getCurrentUser().name,
      to: this.receiver['name'],
      text: message
    });
    this.loadMessageHistory();
  }

  getAllMessages(): Observable<any> {
    return new Observable(observer => {
      this.socket.on('get-messages', data => {
        observer.next(data);
      });
    });
  }

  getUsers() {
    this.socket.emit('get-users');
    return new Observable(observer => {
      this.socket.on('get-users', data => {
        observer.next(data);
      });
    });
  }

  loadMessageHistory(): void {
    this.socket.emit('get-messages', this.userService.getCurrentUser()['name'], this.receiver['name']);
  }
}
