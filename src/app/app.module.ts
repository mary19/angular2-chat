import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { DialogComponent } from './components/dialog/dialog.component';
import {ChatService} from "./services/chat.service";
import { LoginComponent } from './components/login/login.component';
import {UserService} from "./services/user.service";
import { ChatComponent } from './components/chat/chat.component';
import {AppRoutingModule} from "./app-routing.module";
import { OnlineUsersComponent } from './components/online-users/online-users.component';
import { HeaderComponent } from './components/header/header.component';
import {LinkifyPipe} from "./pipes/linkify.pipe";

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
    LoginComponent,
    ChatComponent,
    OnlineUsersComponent,
    HeaderComponent,
    LinkifyPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [ChatService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
