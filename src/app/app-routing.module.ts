/**
 * Created by maryna on 28.06.17.
 */
import {NgModule} from "@angular/core";
import {RouterModule, Routes} from '@angular/router';
import {ChatComponent} from "./components/chat/chat.component";
import {LoginComponent} from "./components/login/login.component";
import {UserService} from "./services/user.service";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'chat',
    component: ChatComponent,
    canActivate: [UserService]
  },
  {
    path: '**',
    redirectTo: '/login'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
